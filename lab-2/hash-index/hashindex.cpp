#include "hashindex.h"

#include <thread>
#include <fstream>
#include <filesystem>
#include <sstream>

#include <iostream>
#include <regex>

#include <atomic>

#include <mutex>

namespace fs = std::filesystem;
const char defaultRecordSeparator = '|';

// Ignore first | and get data to second |
inline FileRecord tableStringToFileRecordTranslate(std::string asString) {
    std::stringstream buffer(asString);
    FileRecord result;
    buffer >> result.key;
    char startTrait;
    buffer >> startTrait;
    std::getline(buffer, result.data, defaultRecordSeparator);
    return result;
}

bool isFileExist(std::string filename) {
    return fs::exists(filename);
}

std::vector<std::string> getFileSortedFileNames() {
    std::regex nameRegex("("+DEFAULT_SECTION_FILE_PREFIX+") \\d+");

    auto filesInFolder = fs::directory_iterator(DEFAULT_SECTIONS_FOLDER_PREFIX);

    std::vector<std::string> fileInFolderNames;

    for (const auto & entry : filesInFolder) {
        if (entry.path().has_filename()) {
            if (std::regex_match(entry.path().filename().c_str(), nameRegex))
                fileInFolderNames.push_back(entry.path().filename().c_str());
        }
    }

    // Упорядочили файлы, теперь можем их открывать по потокам и у каждого формировать свою таблицу
    std::sort(fileInFolderNames.begin(), fileInFolderNames.end(), [](std::string &first, std::string &second) {
        auto indexF = first.find(" "), indexS = second.find(" ");
        return std::stoi(first.substr(indexF+1)) < std::stoi(second.substr(indexS+1));
    });

    return fileInFolderNames;
}


void HashIndex::calculateLasFileNameIndex() noexcept
{
    std::vector<std::string> fileInFolderNames = getFileSortedFileNames();
    auto whiteSpacePos = fileInFolderNames[fileInFolderNames.size() - 1].find(" ");

    lastFileNameIndex = std::stoi(fileInFolderNames[fileInFolderNames.size() - 1].substr(whiteSpacePos+1));
}

HashIndex::HashIndex(std::string &&recordsFileName) : CORES_COUNT(std::thread::hardware_concurrency())
{

    table.resize(DEFAULT_RECORDS_COUNT);
    // Восстановление hash index table
    if (!restore(recordsFileName))
        throw std::invalid_argument("Невозможно построить хэш таблицу (отсутствует кеш файл " + cacheFileName + ", "
                                    "либо файл данных " + recordsFileName + "\n" );

    calculateLasFileNameIndex();
}

HashIndex::~HashIndex()
{
    // Обновление кеша журнала, если надо
    handleJournal();

    if (resaveCache) {
        if (!saveCacheFile()) {
            std::cout << "Error while saving cache file" << std::endl;
        }
    }
}

// Восстановление состояния из кеш файла
bool HashIndex::restoreFromCache() noexcept { // Threads ?
    std::ifstream cacheFile(cacheFileName);

    if (!cacheFile.is_open())
        return false;

    std::stringstream buffer;
    buffer << cacheFile.rdbuf();
    cacheFile.close();

    int count = 0;
    // Считаем все записи из файла
    TableRecord record;
    while (buffer >> record.index >> record.fileIndex >> record.offset >> record.size) {
        table[count++] = record;
    }

    table.resize(count);

    return true;
}

inline Key getIndexFromString(std::string && str) {
    auto whiteSpacePos = str.find(" ", 0);
    return std::stoi(str.substr(0, whiteSpacePos));
}

// Восстановление состояния из файла generated.tb
bool HashIndex::restoreFromDataFile(std::string &recordsFileName) noexcept {
    // Разделим файл на секции и запишем их в папку sections
    // Далее вызовем функцию по восстановлению из секций
    std::ifstream dataFile(recordsFileName);

    if (!dataFile.is_open())
        return false;

    fs::remove_all(DEFAULT_SECTIONS_FOLDER_PREFIX); // Очистим от старых файлов
    fs::create_directory(DEFAULT_SECTIONS_FOLDER_PREFIX);

    const std::regex r("(\\d+\\s\\|.*\\|\n)");

    std::vector<std::thread> workers(CORES_COUNT);

    std::string fileData (std::istreambuf_iterator<char>(dataFile), (std::istreambuf_iterator<char>()));
    std::regex_token_iterator<std::string::iterator> a (fileData.begin(), fileData.end(), r);
    std::regex_token_iterator<std::string::iterator> rend;

    std::atomic_long totalRowCount = 0;

    for (auto i = 0; i < CORES_COUNT; i++) {
        workers[i] = std::thread([&](size_t index) {
                auto thread_a = a;
                // File not end
                std::stringstream record;
                while (thread_a != rend) {
                thread_a = a;
                auto currentStartIndex = index * DEFAULT_SECTION_RECORD_COUNT;
                // std::advance with no ub
                for (auto i = 0; i < currentStartIndex && thread_a != rend; i++) {
            thread_a++;
        };

        if (thread_a == rend) return;

        std::ofstream sectionFile(defaultPath + std::to_string(index + 1));
        auto rowCount = 0, offset = 0;

        while (thread_a != rend && rowCount < DEFAULT_SECTION_RECORD_COUNT) {
            table[rowCount + currentStartIndex] = TableRecord(getIndexFromString(thread_a->str()), index + 1, offset, thread_a->str().size());
            offset += thread_a->str().size();
            sectionFile << *thread_a++;
            rowCount++;
        }

        totalRowCount += rowCount;

        index += CORES_COUNT;
    }}, i);
}

for (auto& i : workers) i.join();

table.resize(totalRowCount);

return true;
}

bool HashIndex::saveCacheFile() const noexcept
{
    std::ofstream cacheFile(cacheFileName);

    if (!cacheFile.is_open())
        return false;

    for (const auto& record : table) {
        cacheFile << record.index <<" "
                  << record.fileIndex << " "
                  << record.offset << " "
                  << record.size << " ";
    }

    return true;
}

void HashIndex::packSections() noexcept
{
    std::vector<std::string> fileInFolderNames = getFileSortedFileNames();

    long needToAdd = 0;

    const std::regex r("(\\d+\\s\\|.*\\|\n)");

    auto fileAsString = [](std::fstream &file) {
        return std::string (std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));
    };

    const auto end_iterator = std::sregex_iterator();

    for (auto i = 0; i < fileInFolderNames.size(); i++) {
        std::fstream file(DEFAULT_SECTIONS_FOLDER_PREFIX + "/" + fileInFolderNames[i]);
        // Посмотрим сколько записей

        const std::regex r("(\\d+\\s\\|.*\\|\n)");

        std::string records = fileAsString(file);

        auto begin_iterator = std::sregex_iterator(records.cbegin(), records.cend(), r);
        auto recordsCount = std::distance(begin_iterator, end_iterator);

        needToAdd = recordsCount - DEFAULT_SECTION_RECORD_COUNT;

        if (i < fileInFolderNames.size() - 1 && needToAdd != 0) {

            if (needToAdd < 0) { // Количество записей в файле МЕНЬШЕ значения по умолчанию - ДОБАВЛЯЕМ ИХ из другого файла
                // Открываем следующий файл
                // Берем из него столько записей, сколько не хватает
                std::fstream nextFile(DEFAULT_SECTIONS_FOLDER_PREFIX + "/" + fileInFolderNames[i + 1]);
                std::string nextFileRecords(std::istreambuf_iterator<char>(nextFile), (std::istreambuf_iterator<char>()));

                auto newf_begin_iterator = std::sregex_iterator(nextFileRecords.cbegin(), nextFileRecords.cend(), r);

                while (needToAdd != 0 && newf_begin_iterator != end_iterator) {
                    file << newf_begin_iterator->str();
                    newf_begin_iterator++;
                    needToAdd++;
                }

                nextFile.close();

                fs::remove(DEFAULT_SECTIONS_FOLDER_PREFIX + "/" + fileInFolderNames[i + 1]);
                std::fstream newNextFile (DEFAULT_SECTIONS_FOLDER_PREFIX + "/" + fileInFolderNames[i + 1]);

                for (; newf_begin_iterator != end_iterator; newf_begin_iterator++) {
                    newNextFile << newf_begin_iterator->str();
                }
            }

            else if (needToAdd > 0) { // Количество записей в файле БОЛЬШЕ значения по умолчанию - ЗАПИСЫВАЕМ ИХ в другой файл
                // Открываем следующий файл
                // Записываем в него столько записей, в скольких перебор
                std::ofstream nextFile(DEFAULT_SECTIONS_FOLDER_PREFIX + "/" + fileInFolderNames[i + 1], std::ios::app);

                auto tempBeginIterator = begin_iterator;
                std::advance(tempBeginIterator, DEFAULT_SECTION_RECORD_COUNT);

                while (needToAdd != 0 && tempBeginIterator != end_iterator) {
                    nextFile << tempBeginIterator->str();
                    tempBeginIterator++;
                    needToAdd--;
                }

                nextFile.close();
                file.close();

                std::ofstream newFile(DEFAULT_SECTIONS_FOLDER_PREFIX + "/" + fileInFolderNames[i]); // re-create

                for (auto count = 0;
                     begin_iterator != end_iterator && count != DEFAULT_SECTION_RECORD_COUNT;
                     begin_iterator++, count++) {

                    newFile << begin_iterator->str();
                }
            }
        }
    }


    if (needToAdd > 0) {

        auto startFileName = fileInFolderNames[fileInFolderNames.size() - 1];

        std::fstream file(DEFAULT_SECTIONS_FOLDER_PREFIX + "/" + startFileName);

        auto records = fileAsString(file);

        auto begin_iterator = std::sregex_iterator(records.cbegin(), records.cend(), r);

        auto countWrites = 0;

        file.close();
        std::ofstream newfile(DEFAULT_SECTIONS_FOLDER_PREFIX + "/" + startFileName); // re-create

        while (countWrites != DEFAULT_SECTION_RECORD_COUNT) {
            newfile << begin_iterator->str();
            begin_iterator++;
            countWrites++;
        }

        while (needToAdd != 0) {

            std::ofstream nextFile(defaultPath + std::to_string(lastFileNameIndex+1));

            while (needToAdd != 0 && begin_iterator != end_iterator) {
                nextFile << begin_iterator->str();
                begin_iterator++;
                needToAdd--;
            }

            nextFile.close();
            startFileName = DEFAULT_SECTIONS_FOLDER_PREFIX + std::to_string(lastFileNameIndex++);
        }
    }
}

void HashIndex::reindexCache() noexcept
{
    handleJournal(true);
    resaveCache = false;

    packSections();
    return;
    // Смотрим на файлы секций
    // Каждый поток читает и обрабатывает свою секцию
    // Как прочитал и сформировал свою таблицу, так записал ее в indexCache.ht

    std::vector<std::string> fileInFolderNames = getFileSortedFileNames();

    std::ofstream cacheFile(cacheFileName); // re-create

    if (!cacheFile.is_open()) {
        std::cout << "Can't recreate cache file " << cacheFileName << std::endl;
        return;
    }

    std::vector<std::thread> workers(CORES_COUNT);

    std::mutex cacheFileMutex;

    const std::regex r("(\\d+\\s\\|.*\\|\n)");

    const auto end_iterator = std::sregex_iterator();

    // Split work for threads
    for (auto i = 0; i < CORES_COUNT; i++) {

        workers[i] = std::thread([&](size_t index) {

                while(true) {

                if (index >= fileInFolderNames.size())
                    return ;

                std::ifstream file(DEFAULT_SECTION_FILE_PREFIX + "/" +fileInFolderNames[index]);
                std::string fileData (std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));
                auto begin_iterator = std::sregex_iterator(fileData.cbegin(), fileData.cend(), r);

                auto offset = 0;

                cacheFileMutex.lock();

                while (begin_iterator != end_iterator) {
                    auto fileRecord = tableStringToFileRecordTranslate(begin_iterator->str());
                    auto formattedString = std::to_string(fileRecord.key) + " |" + fileRecord.data + "|\n";
                    cacheFile << fileRecord.key << index + 1 << offset << formattedString.size();
                    offset += formattedString.size();
                    begin_iterator++;
                }

                cacheFileMutex.unlock();

                index += CORES_COUNT;
        }
    },
                i);
    }

    for (auto &i : workers) i.join();


}

// Восстановление table
//  1. Из кеш файла с предыдущим состоянием
//  2. Файла generated.tb (если кеш файла нет или он пустой)
bool HashIndex::restore(std::string recordsFileName)
{
    if(isFileExist(cacheFileName)) {
        return restoreFromCache();
    }

    resaveCache = true;

    if (isFileExist(recordsFileName)) {
        return restoreFromDataFile(recordsFileName);
    }

    return false;
}

FileRecord* HashIndex::search(Key key) noexcept
{
    TableRecord* tableField = nullptr;
    std::atomic_uint8_t threadEnd = 0;

    // Search
    auto threadSearching = [&](size_t indexStart, size_t recordsCount) {
        for (auto i = indexStart; i < indexStart + recordsCount && tableField == nullptr; i++) {
            if (key == table[i].index) {
                tableField = &table[i];
                return;
            }
        }
        threadEnd++;
    };

    // Using CORES_COUNT threads
    std::vector<std::thread> workers(CORES_COUNT);

    auto additionalIndexesForLastThread = table.size() % CORES_COUNT;
    auto step = table.size() / CORES_COUNT;

    // Split work for threads
    for (auto i = 0; i < CORES_COUNT; i++) {
        auto lastIncrement = 0;

        if (i == CORES_COUNT - 1)
            lastIncrement = additionalIndexesForLastThread;

        workers[i] = std::thread(threadSearching, i * step, step + lastIncrement);
    }

    while (threadEnd != CORES_COUNT - 1 && tableField == nullptr) { }

    for (auto &i : workers) i.join();

    // element not found
    if (tableField == nullptr) {
        return nullptr;
    }

    // From tableRecord to FileRecord
    std::string filePath = defaultPath + std::to_string(tableField->fileIndex);
    std::ifstream sectionFile(filePath);

    if (!sectionFile.is_open()) {
        std::cout << "Search: File " << filePath << " not found!" << std::endl;
        return nullptr;
    }

    char recordAsCString[tableField->size];

    sectionFile.seekg(tableField->offset);

    sectionFile.read(recordAsCString, tableField->size);

    return new FileRecord(tableStringToFileRecordTranslate(std::string(recordAsCString)));
}

TableRecord HashIndex::insert(const FileRecord &&record) noexcept
{
    if (search(record.key) != nullptr) {
        std::cout << "> Update command <" << std::endl;
        return update(record);
    }

    // Если в последнем файле есть место, вставим в последний файл
    // Иначе создадим новый файл и вставим в него

    lastFileNameIndex = (lastFileNameIndex == 0) ? 1 : lastFileNameIndex;

    std::fstream file(defaultPath + std::to_string(lastFileNameIndex));
    // Посмотрим сколько записей

    const std::regex r("(\\d+\\s\\|.*\\|\n)");

    std::string records(std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));

    const auto begin_iterator = std::sregex_iterator(records.cbegin(), records.cend(), r);
    const auto end_iterator = std::sregex_iterator();

    auto recordsCount = std::distance(begin_iterator, end_iterator);

    auto formattedString = std::to_string(record.key) + " |" + record.data + "|\n";

    TableRecord tableRecord;
    tableRecord.index = record.key;
    tableRecord.size =  formattedString.size();

    // Вставим в этот файл
    if (recordsCount < DEFAULT_SECTION_RECORD_COUNT - 1) {
        tableRecord.fileIndex = lastFileNameIndex;
        tableRecord.offset = file.tellg();
    } else {
        tableRecord.fileIndex = ++lastFileNameIndex;
        tableRecord.offset = 0;
        file.close();
        file.open(defaultPath + std::to_string(lastFileNameIndex), std::ios::app);
    }

    file << formattedString;
    table.push_back(tableRecord);

    operationsJournal.push_back(JournalRecord(tableRecord, INSERT));

    return tableRecord;
}

size_t HashIndex::searchFirstFileRecordWithZeroOffset(Key fileIndex) noexcept
{
    std::atomic_uint8_t threadEnd = 0;
    unsigned long tableRecordIndex = ULLONG_MAX;

    // Search
    auto threadSearching = [&](size_t indexStart, size_t recordsCount) {
        for (auto i = indexStart; i < indexStart + recordsCount && tableRecordIndex == ULLONG_MAX; i++) {
            if (fileIndex == table[i].fileIndex && table[i].offset == 0) {
                tableRecordIndex = i;
                return;
            }
        }
        threadEnd++;
    };

    // Using CORES_COUNT threads
    std::vector<std::thread> workers(CORES_COUNT);

    auto additionalIndexesForLastThread = table.size() % CORES_COUNT;
    auto step = table.size() / CORES_COUNT;

    // Split work for threads
    for (auto i = 0; i < CORES_COUNT; i++) {
        auto lastIncrement = 0;

        if (i == CORES_COUNT - 1)
            lastIncrement = additionalIndexesForLastThread;

        workers[i] = std::thread(threadSearching, i * step, i * step + step + lastIncrement);
    }

    while (threadEnd != CORES_COUNT - 1 && tableRecordIndex == ULLONG_MAX) { }

    for (auto &i : workers) i.join();

    return tableRecordIndex;
}

size_t HashIndex::searchRecordIndex(Key key) noexcept
{
    std::atomic_uint8_t threadEnd = 0;
    unsigned long tableRecordIndex = ULLONG_MAX;

    // Search
    auto threadSearching = [&](size_t indexStart, size_t recordsCount) {
        for (auto i = indexStart; i < indexStart + recordsCount && tableRecordIndex == ULLONG_MAX; i++) {
            if (key == table[i].index) {
                tableRecordIndex = i;
                return;
            }
        }
        threadEnd++;
    };

    // Using CORES_COUNT threads
    std::vector<std::thread> workers(CORES_COUNT);

    auto additionalIndexesForLastThread = table.size() % CORES_COUNT;
    auto step = table.size() / CORES_COUNT;

    // Split work for threads
    for (auto i = 0; i < CORES_COUNT; i++) {
        auto lastIncrement = 0;

        if (i == CORES_COUNT - 1)
            lastIncrement = additionalIndexesForLastThread;

        workers[i] = std::thread(threadSearching, i * step, i * step + step + lastIncrement);
    }

    while (threadEnd != CORES_COUNT - 1 && tableRecordIndex == ULLONG_MAX) { }

    for (auto &i : workers) i.join();

    return tableRecordIndex;
}

void HashIndex::handleJournal(bool callFromReindexCache) noexcept
{
    for (auto &i : operationsJournal) {
        if (i.state == UPDATE || i.state == REMOVE) {
            // Из файла стерли старую запись
            // Нашли индекс в таблице, с которой начинается запись данного файла
            // Перечитали запись из файла в таблицу
            std::fstream fileWithRecord(defaultPath + std::to_string(i.record.fileIndex), std::ios::in | std::ios::out);

            // Затрем старую запись в файле пустотой
            std::vector<std::string> recordsFromFile;

            fileWithRecord.seekp(0);

            std::string str;

            std::ofstream fileCopy(defaultPath + "_" + std::to_string(i.record.fileIndex));

            bool recordDeleted = false;

            auto isEmpty = true;

            while (std::getline(fileWithRecord, str)) {
                if (recordDeleted) {
                    fileCopy << str << "\n";
                    isEmpty = false;
                } else {
                    if (tableStringToFileRecordTranslate(str).key == i.record.index) {
                        recordDeleted = true;
                    } else {
                        fileCopy << str << "\n";
                        isEmpty = false;
                    }
                }
            }

            fileWithRecord.close();
            fileCopy.close();

            fs::remove(defaultPath + std::to_string(i.record.fileIndex));

            if (isEmpty) {
                fs::remove(defaultPath + "_" + std::to_string(i.record.fileIndex));
                return;
            }

            fs::rename(defaultPath + "_" + std::to_string(i.record.fileIndex), defaultPath + std::to_string(i.record.fileIndex));

            std::fstream updatedFile(defaultPath + std::to_string(i.record.fileIndex));

            // Пройти по файлу, взять индексы и поискав их в таблице обновить поля таблицы новым состоянием файла
            resaveCache = true;

            const std::regex r("(\\d+\\s\\|.*\\|\n)");

            std::string records(std::istreambuf_iterator<char>(updatedFile), (std::istreambuf_iterator<char>()));

            const auto begin_iterator = std::sregex_iterator(records.cbegin(), records.cend(), r);
            const auto end_iterator = std::sregex_iterator();

            auto threadWork = [&](std::sregex_iterator begin, size_t start, size_t count){
                std::advance(begin, start);
                for (auto i = 0; i < count && begin != end_iterator; i++) {

                    auto parsedValue = tableStringToFileRecordTranslate(begin->str());
                    auto itemIndex = searchRecordIndex(parsedValue.key);

                    if (itemIndex == ULLONG_MAX) continue;

                    table[itemIndex].offset = begin->position();
                    table[itemIndex].size = begin->str().size();
                    table[itemIndex].index = parsedValue.key;

                    begin++;
                }
            };

            // Using CORES_COUNT threads
            std::vector<std::thread> workers(CORES_COUNT);

            auto size = std::distance(begin_iterator, end_iterator);

            auto additionalIndexesForLastThread = size % CORES_COUNT;
            auto step = size / CORES_COUNT;

            // Split work for threads
            for (auto i = 0; i < CORES_COUNT; i++) {
                auto lastIncrement = 0;

                if (i == CORES_COUNT - 1)
                    lastIncrement = additionalIndexesForLastThread;

                auto thIterator = begin_iterator;
                workers[i] = std::thread(threadWork, thIterator, i * step, step + lastIncrement);
            }

            for (auto &i : workers) i.join();

        }

        if (i.state == INSERT) {

        }
    }

    operationsJournal.clear();
}

size_t HashIndex::searchIndexSequentially(Key key)
{
    // Search
    auto tableField = std::find_if(table.begin(), table.end(), [&](TableRecord &record){ return record.index == key; });

    return std::distance(tableField, table.begin());
}

TableRecord HashIndex::update(const FileRecord record) noexcept
{
    auto oldRecordIndex = searchRecordIndex(record.key);
    auto tableRecord = table[oldRecordIndex];

    // Нет такого
    if (tableRecord.index != record.key) {
        std::cout << "Record with key " << record.key << " not found and can't be updated!" << std::endl;
        return TableRecord::getUnavailableRecord();
    }

    // Вставим в файл со старой записью новую запись в конец
    std::ofstream sectionFile(defaultPath + std::to_string(tableRecord.fileIndex), std::ios::app);

    if (!sectionFile.is_open()) {
        std::cout << "Can't open file " << defaultPath + std::to_string(tableRecord.fileIndex) << std::endl;
        return TableRecord::getUnavailableRecord();
    }

    auto formattedString = std::to_string(record.key) + " |" + record.data + "|\n";

    TableRecord updated(record.key, tableRecord.fileIndex, sectionFile.tellp(), formattedString.size());

    sectionFile << formattedString;
    sectionFile.close();

    operationsJournal.push_back(JournalRecord(tableRecord, UPDATE));

    table[oldRecordIndex] = updated;

    return updated;
}

TableRecord HashIndex::update(Key key, std::string &&data) noexcept
{
    return update(FileRecord(key, data));
}

bool HashIndex::remove(Key key) noexcept
{
    auto elementIndex = searchRecordIndex(key);

    if (elementIndex == ULLONG_MAX) {
        std::cout << "Can't find record with key "<< key << std::endl;
        return false;
    }

    operationsJournal.push_back(JournalRecord(table[elementIndex], REMOVE));
    // resaveCache = true;

    table.erase(table.begin() + elementIndex);

    return true;
}
