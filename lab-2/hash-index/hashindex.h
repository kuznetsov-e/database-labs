#ifndef HASHINDEX_H
#define HASHINDEX_H

#include <vector>
#include <string>
#include <thread>
#include <fstream>

using Key = long;

// Элемент таблицы в оперативной памяти
struct TableRecord {
    Key index;
    uint32_t fileIndex = 0; /// ! Новое поле - указывает на файл, где читать
    unsigned long offset;
    uint16_t size;

    TableRecord(Key index, uint32_t fileIndex, unsigned long offset, uint16_t size) {
        this->index = index;
        this->fileIndex = fileIndex;
        this->offset = offset;
        this->size = size;
    }

    TableRecord() = default;

    bool isTableRecordHandledSuccesfully() {
        return !(getUnavailableRecord() == *this);
    }

    bool operator==(TableRecord record){
        return record.fileIndex == fileIndex && record.index == index &&
                record.offset == offset && record.size == size;
    }

    static TableRecord getUnavailableRecord() {
        static TableRecord record (0, 0, 0, 0);
        return record;
    }
};

// Запись в файле, которую будем читать из файла
struct FileRecord {
    Key key;
    std::string data;
    FileRecord(Key key, std::string data) {
        this->key = key;
        this->data = data;
    }
    FileRecord() { }
};

using Table = std::vector<TableRecord>;

const uint32_t DEFAULT_RECORDS_COUNT = 10000000; // change to 100 mills later
const uint32_t DEFAULT_SECTION_RECORD_COUNT = 100000;
const std::string DEFAULT_SECTIONS_FOLDER_PREFIX = "sections";
const std::string DEFAULT_SECTION_FILE_PREFIX = "section";

enum JournalRecordState : uint8_t { UPDATE, REMOVE, INSERT };

struct JournalRecord {
    TableRecord record;
    JournalRecordState state;

    JournalRecord(TableRecord record, JournalRecordState state) {
        this->record = record;
        this->state = state;
    }
};

using Journal = std::vector<JournalRecord>;

class HashIndex {
private:
    Table table;
    Journal operationsJournal;

    const std::string cacheFileName = "indexCache.ht";
    // Для работы с потоками узнаем их количество на машине
    uint8_t CORES_COUNT = 0;

    // Флаг обновления файла кеша
    // True - insert, remove, update, инициализация не из кеш файла
    bool resaveCache = false;

    std::string defaultPath = DEFAULT_SECTIONS_FOLDER_PREFIX + "/" + DEFAULT_SECTION_FILE_PREFIX + " ";

    // Восстановление table
    //  1. Из кеш файла с предыдущим состоянием
    //  2. Файла generated.tb (если кеш файла нет или он пустой)
    bool restore(std::string);

    bool restoreFromCache() noexcept;
    bool restoreFromDataFile(std::string &recordsFileName) noexcept;

    // Сохранение хэш таблицы
    bool saveCacheFile() const noexcept;

    void packSections() noexcept;

    // Чтобы сохранить в последний файл новую запись
    long lastFileNameIndex = 0;
    void calculateLasFileNameIndex() noexcept;

    size_t searchFirstFileRecordWithZeroOffset(Key) noexcept;
    size_t searchRecordIndex(Key) noexcept;

    void handleJournal(bool callFromReindexCache = false) noexcept;

    size_t searchIndexSequentially(Key);
public:
    HashIndex(std::string &&recordsFileName = "generated.tb");
    ~HashIndex();

    void reindexCache() noexcept;

    FileRecord* search(Key) noexcept;
    TableRecord insert(const FileRecord&&) noexcept;
    TableRecord update(const FileRecord) noexcept;
    TableRecord update(Key, std::string&&) noexcept;
    bool remove(Key) noexcept;
};

#endif // HASHINDEX_H
