#include <iostream>
#include "hashindex.h"

using namespace std;

void menu(HashIndex &index) {
    std::cout << "Hash table commands: \n"
              << "\tS(earch) <N>         - search record with index N\n"
              << "\tI(nsert) <N> <data>  - to insert record with index N and value \'data\'\n"
              << "\tR(emove) <N>         - to remove element with index N\n";
              // << "\tC(cache reindex)     - to reindex cache file with existing sections\n";

    while (true) {

        long N = 0;
        std::string data = "";
        char command;

        std::cin >> command;

        if (command == 'E') {
            std::cout << "> Saving state and exit <\n";
            break;
        }

//        if (command == 'C') {
//            std::cout << "> Reindex table and update cache file <\n";
//            index.reindexCache();
//            break;
//        }
        else

        std::cin >> N;

        if (!std::cin.good()) {
            std::cout << "> Second param must be an integer <\n";
            std::cin.clear();
            continue;
        }

        if (command == 'I') {

            std::getline(std::cin, data);

            auto res = index.insert(FileRecord(N, data));

            if (res.isTableRecordHandledSuccesfully()) {
                std::cout << "> Record was succesfully inserted! <\n";
            } else {
                std::cout << "> Record was not inserted! <\n";
            }
        }

        else

        if (command == 'S') {
            auto res = index.search(N);

            if (res != nullptr) {
                std::cout << "> Record was found: " << res->key << " \"" << res->data <<"\" <\n";
            } else {
                std::cout << "> Record was not found! <\n";
            }
        }

        else

        if (command == 'R') {
            auto res = index.remove(N);

            if (res) {
                std::cout << "> Record was successfully removed! <\n";
            } else {
                std::cout << "> Record was not removed! <\n";
            }
        }

        else
        std::cout << "> Command " << command << " not found! <\n";

        std::cin.clear();
    }
}


int main()
{
    HashIndex index;

    menu(index);

    return 0;
}
