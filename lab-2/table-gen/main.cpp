#include <iostream>
#include <vector>
#include <thread>
#include <fstream>
#include <random>

using namespace std;

auto recordCount = 100'000'000; // by default
auto threadCount = 10;          // by default

auto onePartCount = 1'000'000;  // by default

constexpr auto dictionarySize = 200'000;

// Standart *NIX system dictionary
const auto wayToDictionary = "/usr/share/dict/words";

auto dictionary = std::vector<std::string>(dictionarySize);

char filename[] = "generated.tb";

std::random_device rd;
std::mt19937 gen(rd()); // seed the generator
std::uniform_int_distribution<> distr(0, dictionarySize); // define the range

struct record {
    std::string value;  // WORD1-WORD2

    public:

    record(std::string value) {
        this->value = value;
    }

    record() = default;
};

void initDictionary() noexcept {
    std::ifstream dictFile(wayToDictionary);

    for (auto i = 0 ; i < dictionarySize; i++) {
        std::getline(dictFile, dictionary[i]);
    }
}

void writeToFile(std::ofstream& file, std::vector<record>& records) noexcept {

    static long long index = 0;

    for (auto& i : records) {
        file << ++index << " |" << i.value << "|\n";
    }
}

inline std::string generateValue() {
    return dictionary[distr(gen)] + "-" + dictionary[distr(gen)];
}

void generateDataPart(std::vector<record> &part, uint32_t start, uint32_t end) noexcept {

    for (; start < end; start++) {
        part[start].value = generateValue();
    }
}

void generateData() noexcept {
    std::ofstream file(filename);

    auto inc = onePartCount / threadCount;

    for (auto i = 0; i < recordCount; i += onePartCount) {

        auto recordPart = std::vector<record>(onePartCount);
        auto threads = std::vector<std::thread>(threadCount);
        for (auto j = 0, t = 0; j < onePartCount; j += inc, t++) {
            threads[t] = std::thread(std::bind(generateDataPart, std::ref(recordPart), j, j + inc));
        }

        for (auto& th : threads)
            th.join();

        writeToFile(file, recordPart);
    }
}

int main(int argc, char* argv[])
{
    std::cout << "Parameters:\n"
              << "  [optinal] arg[1]: total records count (default 100'000'000) \n"
              << "  [optinal] arg[2]: thread count (default 10) \n"
              << "  [optinal] arg[3]: output file name (default generated.tb) \n"
              << "Example: ./table-gen 100'000 10 final.tb - write 100'000 generated records on 10 threads in final.tb\n\n";

    switch (argc) {
        case 4: { strncpy(filename, argv[3], strlen(argv[3])); }
        case 3: { threadCount = std::atoi(argv[2]); }
        case 2: { recordCount = std::atoi(argv[1]); }
        case 1: { onePartCount = recordCount / (threadCount); }
    }

    initDictionary();

    std::cout << " -- Start generating --- \n";
    generateData();
    std::cout << " -- End generating. " << "Results in " << filename << " --- \n";
    return 0;
}
