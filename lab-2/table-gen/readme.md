to build:

mkdir build
cd build
cmake ..
cmake --build .

(Generated file in build folder)

to run:

Parameters:
	[optinal] arg[1]: total records count (default 100'000'000)
 	[optinal] arg[2]: thread count (default 10)
	[optinal] arg[3]: output file name (default generated.tb)

Example: 
	./table-gen 100'000 10 final.tb - write 100'000 generated records on 10 threads in final.tb